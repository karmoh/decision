$("#decision-form").submit(function (event) {
    event.preventDefault();
    var json = ConvertFormToJSON($(this));

    $.ajax({
        type: "POST",
        data: JSON.stringify(json),
        contentType: "application/json",
        dataType: "json",
        cache: false,
        url: "/loans",
        success: function (data) {
            alert('Decision: ' + data.data.decision + ' on ' + data.data.loanAmount + ' sand dollars');
        }
    });

    return true;
});

function ConvertFormToJSON(form) {
    var array = jQuery(form).serializeArray();
    var json = {};
    jQuery.each(array, function () {
        json[this.name] = this.value || '';
    });
    return json;
}
