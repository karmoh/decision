package com.example.decision.web;

import com.example.decision.dto.LoanRequest;
import com.example.decision.dto.LoanResponse;
import com.example.decision.dto.ServiceResponse;
import com.example.decision.service.LoanService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
public class LoanController {

    private final LoanService loanService;

    @PostMapping("loans")
    public ServiceResponse<LoanResponse> getLoanDecision(@Valid @RequestBody LoanRequest request) {
        return ServiceResponse.ok(loanService.getLoanDecision(request));
    }

}
