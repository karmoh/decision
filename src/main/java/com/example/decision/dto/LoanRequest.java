package com.example.decision.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoanRequest {

    @NotNull
    @Max(10000)
    @Min(2000)
    private BigDecimal loanAmount;
    @NotBlank
    private String personalCode;
    @NotNull
    @Max(60)
    @Min(12)
    private Integer loanPeriod;

}
