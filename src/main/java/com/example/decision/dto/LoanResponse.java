package com.example.decision.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoanResponse {

    private BigDecimal loanAmount;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private BigDecimal maximumLoanForPeriod;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer minimumPeriodForLoanAmount;
    private DecisionType decision;
    private Integer loanPeriod;

}
