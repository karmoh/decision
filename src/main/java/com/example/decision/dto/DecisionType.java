package com.example.decision.dto;

public enum DecisionType {
    NEGATIVE,
    POSITIVE
}
