package com.example.decision.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ServiceResponse<T> {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T data;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long total;

    public static <T> ServiceResponse<T> ok() {
        return new ServiceResponse<>();
    }

    public static <T> ServiceResponse<T> ok(@Nullable T data) {
        ServiceResponse<T> response = new ServiceResponse<>();
        response.setData(data);
        return response;
    }

    public static <T> ServiceResponse<T> ok(T data, long total) {
        ServiceResponse<T> response = new ServiceResponse<>();
        response.setData(data);
        response.setTotal(total);
        return response;
    }

}
