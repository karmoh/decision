package com.example.decision.service;

import com.example.decision.dto.DecisionType;
import com.example.decision.dto.LoanRequest;
import com.example.decision.dto.LoanResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Service
@AllArgsConstructor
public class LoanService {

    private final CreditService creditService;

    public LoanResponse getLoanDecision(LoanRequest request) {
        LoanResponse loanResponse = LoanResponse.builder()
                .loanAmount(request.getLoanAmount())
                .decision(DecisionType.NEGATIVE)
                .loanPeriod(request.getLoanPeriod())
                .build();

        BigDecimal creditModifier = creditService.getPersonCreditModifier(request.getPersonalCode());
        if (creditModifier == null) {
            return loanResponse;
        }

        BigDecimal creditScore = getCreditScore(request, creditModifier);
        if (BigDecimal.ONE.compareTo(creditScore) <= 0) {
            loanResponse.setDecision(DecisionType.POSITIVE);
        }

        loanResponse.setMaximumLoanForPeriod(maximumLoanForPeriod(creditModifier, request.getLoanPeriod()));
        loanResponse.setMinimumPeriodForLoanAmount(minimumPeriodForAmount(creditModifier, request.getLoanAmount()));
        return loanResponse;
    }

    private BigDecimal getCreditScore(LoanRequest request, BigDecimal creditModifier) {
        BigDecimal divide = creditModifier.divide(request.getLoanAmount(), 5, RoundingMode.HALF_UP);
        return divide.multiply(BigDecimal.valueOf(request.getLoanPeriod())).setScale(4, RoundingMode.HALF_UP);
    }

    private BigDecimal maximumLoanForPeriod(BigDecimal creditModifier, Integer loanPeriod) {
        BigDecimal maxLoanForPeriod = creditModifier.multiply(BigDecimal.valueOf(loanPeriod));
        if (maxLoanForPeriod.compareTo(BigDecimal.valueOf(10000)) > 0) {
            return BigDecimal.valueOf(10000);
        }
        if (maxLoanForPeriod.compareTo(BigDecimal.valueOf(2000)) < 0) {
            return null;
        }
        return maxLoanForPeriod;
    }

    private Integer minimumPeriodForAmount(BigDecimal creditModifier, BigDecimal loanAmount) {
        int minimumMonth = loanAmount.divide(creditModifier, 0, RoundingMode.CEILING).intValue();
        if (minimumMonth < 12) {
            return 12;
        }
        if (minimumMonth > 60) {
            return null;
        }
        return minimumMonth;
    }

}
