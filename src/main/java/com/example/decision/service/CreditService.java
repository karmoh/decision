package com.example.decision.service;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class CreditService {

    public BigDecimal getPersonCreditModifier(String idCode) {
        return switch (idCode) {
            case "49002010976" -> BigDecimal.valueOf(100);
            case "49002010987" -> BigDecimal.valueOf(300);
            case "49002010998" -> BigDecimal.valueOf(1000);
            default -> null;
        };
    }
}
