package com.example.decision.config.error;

public enum ErrorCodes {
    TECHNICAL_EXCEPTION,
    INVALID_PARAMS;

    public String getErrorCode() {
        return this.name().toLowerCase();
    }

}
