package com.example.decision.config.error;

import com.example.decision.config.error.exception.TechnicalException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.invoke.MethodHandles;
import java.time.Clock;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@ControllerAdvice
@RequestMapping(produces = "application/json")
public class ErrorControllerAdvice {
    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorMessage> bindException(final MethodArgumentNotValidException e) {
        List<String> errors = new ArrayList<>();
        e.getBindingResult().getAllErrors().forEach(fieldError -> {
            FieldError error = (FieldError) fieldError;
            errors.add(error.getField() + " " + error.getDefaultMessage());
        });
        return error(e, ErrorCodes.INVALID_PARAMS.getErrorCode(), HttpStatus.NOT_ACCEPTABLE, errors.toArray());
    }

    @ExceptionHandler({TechnicalException.class, Exception.class})
    public ResponseEntity<ErrorMessage> technicalException(final Exception exception) {
        String uuid = UUID.randomUUID().toString();
        ErrorMessage responseMessage = new ErrorMessage(uuid, ErrorCodes.TECHNICAL_EXCEPTION.getErrorCode(), LocalDateTime.now(Clock.systemUTC()), null);
        LOGGER.error(responseMessage.toString(), exception);
        return new ResponseEntity<>(responseMessage, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<ErrorMessage> error(
            final Exception exception,
            String messageCode,
            HttpStatus httpStatus,
            Object... arguments) {
        ErrorMessage responseMessage = new ErrorMessage(UUID.randomUUID().toString(), messageCode, LocalDateTime.now(Clock.systemUTC()), arguments);
        LOGGER.error(responseMessage.toString(), exception);
        return new ResponseEntity<>(responseMessage, httpStatus);
    }
}
