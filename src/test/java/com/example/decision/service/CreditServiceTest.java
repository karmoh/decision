package com.example.decision.service;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class CreditServiceTest extends AbstractServiceTest<CreditService> {

    @Test
    void getPersonCreditModifier() {
        assertEquals(BigDecimal.valueOf(100), createService().getPersonCreditModifier("49002010976"));
        assertEquals(BigDecimal.valueOf(300), createService().getPersonCreditModifier("49002010987"));
        assertEquals(BigDecimal.valueOf(1000), createService().getPersonCreditModifier("49002010998"));
        assertNull(createService().getPersonCreditModifier("49002010965"));
    }

    @Override
    protected CreditService createService() {
        return new CreditService();
    }
}