package com.example.decision.service;

import com.example.decision.dto.DecisionType;
import com.example.decision.dto.LoanRequest;
import com.example.decision.dto.LoanResponse;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class LoanServiceTest extends AbstractServiceTest<LoanService> {

    @Mock
    private CreditService creditService;

    @Test
    void requestLoanPositive() {
        when(creditService.getPersonCreditModifier("123")).thenReturn(BigDecimal.valueOf(500));
        LoanResponse result = createService().getLoanDecision(LoanRequest.builder()
                .loanAmount(BigDecimal.valueOf(6000))
                .personalCode("123")
                .loanPeriod(24)
                .build());
        assertEquals(BigDecimal.valueOf(6000), result.getLoanAmount());
        assertEquals(BigDecimal.valueOf(10000), result.getMaximumLoanForPeriod());
        assertEquals(12, result.getMinimumPeriodForLoanAmount());
        assertEquals(DecisionType.POSITIVE, result.getDecision());
        assertEquals(24, result.getLoanPeriod());
    }


    @Test
    void requestLoanNegative() {
        when(creditService.getPersonCreditModifier("123")).thenReturn(BigDecimal.valueOf(300));
        LoanResponse result = createService().getLoanDecision(LoanRequest.builder()
                .loanAmount(BigDecimal.valueOf(7000))
                .personalCode("123")
                .loanPeriod(14)
                .build());
        assertEquals(BigDecimal.valueOf(7000), result.getLoanAmount());
        assertEquals(BigDecimal.valueOf(4200), result.getMaximumLoanForPeriod());
        assertEquals(24, result.getMinimumPeriodForLoanAmount());
        assertEquals(DecisionType.NEGATIVE, result.getDecision());
        assertEquals(14, result.getLoanPeriod());
    }

    @Override
    protected LoanService createService() {
        return new LoanService(creditService);
    }
}