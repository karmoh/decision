package com.example.decision.integration;

import com.example.decision.DecisionApplication;
import io.restassured.config.DecoderConfig;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import io.restassured.module.mockmvc.config.RestAssuredMockMvcConfig;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.context.WebApplicationContext;


@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = DecisionApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public abstract class IntegrationTestBase {
    @Autowired
    private WebApplicationContext context;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        RestAssuredMockMvc.config = RestAssuredMockMvcConfig.config().decoderConfig(DecoderConfig.decoderConfig().defaultContentCharset("UTF-8"));
        RestAssuredMockMvc.webAppContextSetup(context);

    }

    @AfterEach
    public void cleanUp() {
        RestAssuredMockMvc.reset();
    }


}
