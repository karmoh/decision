package com.example.decision.integration;

import com.example.decision.dto.LoanRequest;
import io.restassured.http.ContentType;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;

class LoanITest extends IntegrationTestBase {
    private static final String SERVICE_PATH = "loans";


    @Test
    void loanRequest() {
        LoanRequest loanRequest = LoanRequest.builder()
                .loanAmount(BigDecimal.valueOf(10000))
                .personalCode("49002010998")
                .loanPeriod(36)
                .build();

        RestAssuredMockMvc
                .given()
                .body(loanRequest)
                .contentType(ContentType.JSON)
                .when()
                .post(SERVICE_PATH)
                .then()
                .log().ifError()
                .statusCode(HttpStatus.OK.value())
                .body("data.loanAmount", equalTo(10000))
                .body("data.maximumLoanForPeriod", equalTo(10000))
                .body("data.minimumPeriodForLoanAmount", equalTo(12))
                .body("data.decision", equalTo("POSITIVE"))
                .body("data.loanPeriod", equalTo(36));

    }

    @Test
    void loanRequestNegativeDecisionDept() {
        LoanRequest loanRequest = LoanRequest.builder()
                .loanAmount(BigDecimal.valueOf(5000))
                .personalCode("49002010965")
                .loanPeriod(12)
                .build();

        RestAssuredMockMvc
                .given()
                .body(loanRequest)
                .contentType(ContentType.JSON)
                .when()
                .post(SERVICE_PATH)
                .then()
                .log().all()
                .statusCode(HttpStatus.OK.value())
                .body("data.loanAmount", equalTo(5000))
                .body("data.decision", equalTo("NEGATIVE"))
                .body("data.loanPeriod", equalTo(12));

    }

    @Test
    void loanRequestNegativeDecisionToShortPeriod() {
        LoanRequest loanRequest = LoanRequest.builder()
                .loanAmount(BigDecimal.valueOf(5000))
                .personalCode("49002010976")
                .loanPeriod(24)
                .build();

        RestAssuredMockMvc
                .given()
                .body(loanRequest)
                .contentType(ContentType.JSON)
                .when()
                .post(SERVICE_PATH)
                .then()
                .log().all()
                .statusCode(HttpStatus.OK.value())
                .body("data.loanAmount", equalTo(5000))
                .body("data.maximumLoanForPeriod", equalTo(2400))
                .body("data.minimumPeriodForLoanAmount", equalTo(50))
                .body("data.decision", equalTo("NEGATIVE"))
                .body("data.loanPeriod", equalTo(24));

    }

    @Test
    void loanRequestInvalidParams() {
        LoanRequest loanRequest = LoanRequest.builder()
                .loanAmount(BigDecimal.valueOf(20000))
                .loanPeriod(364)
                .build();

        RestAssuredMockMvc
                .given()
                .body(loanRequest)
                .contentType(ContentType.JSON)
                .when()
                .post(SERVICE_PATH)
                .then()
                .log().all()
                .statusCode(HttpStatus.NOT_ACCEPTABLE.value())
                .body("errorCode", equalTo("invalid_params"))
                .body("arguments", containsInAnyOrder("loanAmount must be less than or equal to 10000",
                        "personalCode must not be blank",
                        "loanPeriod must be less than or equal to 60"));

    }

}
