# Decision engine

## Running application

```
gradlew bootRun
```

Go to [Swagger](http://localhost:8080/swagger-ui.html) resources

Go to http://localhost:8080/ for application

### Prerequisites

You will need Java 14

## Running the tests

```
gradlew test
```

## Built With

* Java14
* Spring Boot
* Lombok
* SpringFox
* REST Assured
